﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace MergeLogFiles
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            string logFileName = ConfigurationManager.AppSettings.Get(@"logFileName");

            string cwd = Directory.GetCurrentDirectory();

            string mergedFile = MergeLogFiles(cwd, logFileName);

            if (mergedFile == null)
            {
                return;
            }

            string appForOpenLogFile = ConfigurationManager.AppSettings.Get(@"appToOpenLogFile");

            if (File.Exists(appForOpenLogFile))
            {
                System.Diagnostics.Process.Start(appForOpenLogFile, mergedFile);
            }
            else
            {
                appForOpenLogFile = @"C:\Program Files (x86)\Notepad++\notepad++.exe";
                if (File.Exists(appForOpenLogFile))
                {
                    System.Diagnostics.Process.Start(appForOpenLogFile, mergedFile);
                }
                else
                {
                    appForOpenLogFile = @"C:\Program Files\Notepad++\notepad++.exe";
                    if (File.Exists(appForOpenLogFile))
                    {
                        System.Diagnostics.Process.Start(appForOpenLogFile, mergedFile);
                    }
                    else
                    {
                        System.Diagnostics.Process.Start(mergedFile);
                    }
                }
            }
        }

        private static string MergeLogFiles(string cwd, string logFileName)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(cwd);
            IEnumerable<FileInfo> serviceLogFiles = dirInfo.GetFiles(logFileName + "*", SearchOption.TopDirectoryOnly);
            if (serviceLogFiles.Count() == 0)
            {
                Console.WriteLine("not found ContextService_Log file.");
                return null;
            }

            string tempFolder = Path.Combine(@"C:\temp\", DateTime.Now.ToString("yyyyMMddHHmmss"));
            Directory.CreateDirectory(tempFolder);

            foreach (FileInfo serviceLogFile in serviceLogFiles)
            {
                File.Copy(serviceLogFile.FullName, Path.Combine(tempFolder, serviceLogFile.Name), true);
            }

            dirInfo = new DirectoryInfo(tempFolder);
            IEnumerable<FileInfo> tmpLogFiles = dirInfo.GetFiles(logFileName + "*", SearchOption.AllDirectories).OrderBy(f => f.LastWriteTime);
            if (tmpLogFiles.Count() == 0)
            {
                Console.WriteLine("not found ContextService_Log file.");
                return null;
            }

            string mergedFile = Path.Combine(tempFolder, "mergedLogFile.txt");
            using (FileStream output = File.Create(mergedFile))
            {
                foreach (FileInfo tmpLogFile in tmpLogFiles)
                {
                    using (FileStream input = File.OpenRead(tmpLogFile.FullName))
                    {
                        input.CopyTo(output);
                    }
                }
            }
            return mergedFile;
        }
    }
}